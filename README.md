# assignment-1-online-portfolio

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Online portfolio for Noroff Accelerate course - FrontEnd development with JavaScript
Unfrotunately, did not have enough time to complete the first optional assignment.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
```

## Usage

```
```

## Maintainers

[@stian.noroff](https://gitlab.com/stian.noroff)

## Contributing



Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Stian Ingebrigtsen
